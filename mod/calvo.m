% stephane.adjemian@ens.fr (2010)

clear all, close all

addpath ../mat                         % directory with matlab files
format long 

ModelName = 'calvo';

write_dynare_files = 0;
% write_static_file = 0;
% write_dynamic_file = 0;
% write_dynamic_file_conditionally_on_expectations = 0;
% write_multivariate_polynomial_file = 0;
% Set task specific number of threads
% setenv('STATIC_MODEL_NUM_THREADS','1');
% setenv('DYNAMIC_MODEL_NUM_THREADS','1');
% setenv('MULTIVARIATE_POLYNOMIAL_NUM_THREADS','1');



%% Variables and parameters declarations.

% Names of the exogenous state variables.
variables = { 'Efficiency', 'RiskPremium' }; 
nn(1) = length(variables);
% Names of the endogenous state variables.
variables = [ variables , { 'Dist', 'Theta' } ];
nn(2)= length(variables)-nn(1);
% Names of the control variables.
variables = [ variables , { 'Consumption', 'Lambda', 'RealWage', 'Inflation', 'NominalInterestRate', 'Z1', 'Z2', 'Z3', 'Output', 'RelativePrice', 'Hours' } ];
nn(3) = length(variables)-sum(nn(1:2));
% Names of the expectation terms.
variables = [ variables , { 'Exp1', 'Exp2', 'Exp3', 'Exp4' } ];
nn(4) = length(variables)-sum(nn(1:3));

% Names of the innovations.
innovations = { 'ea', 0.0077 , '\varepsilon^A'};
innovations = [ innovations ; { 'eb', 0.0055, '\varepsilon^{\Gamma}'} ];

% Declare the names of the parameters and definie initial calibration.
parameters = { 'SIGMAC', 1.5};
parameters = [ parameters ; { 'XIH', NaN} ]; % To be updated with steady state restriction.
parameters = [ parameters ; { 'ETA', 2.0} ];
parameters = [ parameters ; { 'BETA', 0.99} ];
parameters = [ parameters ; { 'NU', 0.75} ];
parameters = [ parameters ; { 'PSI', -0.05} ];
parameters = [ parameters ; { 'EPSILON', 6.0} ];
parameters = [ parameters ; { 'RHOA', 0.85} ];
parameters = [ parameters ; { 'RHOB', 0.85} ];
parameters = [ parameters ; { 'GAMMAPI', 1.2} ];
parameters = [ parameters ; { 'PHI', 6.0*(1-0.05)/(6.0*(1-0.05)-1)} ]; % epsilon*(1+psi)/(epsilon*(1+psi)-1)
% Declare parameters related to the steady state 
for i=1:sum(nn)
    parameters = [ parameters ; { [ variables{i} '_ss' ], NaN} ];
end

%===< Total number of variables >
n = sum(nn);
%===< Create symbolic variables >
create_symbolic_variables(variables,nn);
%===< Create symbolic variables >
create_symbolic_innovations(innovations);
%===< Create symbolic coefficients >
create_symbolic_parameters(parameters);


%% Model declaration (with a Taylor rule).

DynamicModel = [
    Efficiency/Efficiency_ss - exp(ea)*(Efficiency_B/Efficiency_ss)^RHOA ;                                                           % Eq.  1(1)
    RiskPremium/RiskPremium_ss - exp(eb)*(RiskPremium_B/RiskPremium_ss)^RHOB ;                                                       % Eq.  2(2)
    Lambda-Consumption^(-SIGMAC) ;                                                                                                   % Eq.  3(3)
    XIH*Hours^ETA+Lambda*RealWage ;                                                                                                  % Eq.  4(4)
    BETA*RiskPremium*Exp1_F-Lambda/NominalInterestRate ;                                                                             % Eq.  5
    -Z1 + RealWage/Efficiency*Theta^(-PHI)*Output + NU*BETA*Exp2_F/Lambda ;                                                          % Eq.  6(5)
    -Z2 + Theta^(-PHI)*Output + NU*BETA*Exp3_F/Lambda ;                                                                              % Eq.  7(6)
    -Z3 + Output + NU*BETA*Exp4_F/Lambda ;                                                                                           % Eq.  8(7)
    Z2/((1+PSI)*(1-PHI))*RelativePrice^(PHI/(1-PHI)) + Z3*PSI/(1+PSI) + Z1*PHI/((1+PSI)*(PHI-1))*RelativePrice^(PHI/(1-PHI)-1) ;     % Eq.  9(8)
    -Theta + (1-NU)*RelativePrice^(1/(1-PHI)) + NU*(Inflation_ss/Inflation)^(1/(1-PHI))*Theta_B;                                     % Eq. 10(9)
    -((1+PSI) - Theta^(1-PHI)) + (1-NU)*RelativePrice*PSI + NU*Inflation_ss/Inflation*((1+PSI) - Theta_B^(1-PHI)) ;                  % Eq. 11(10)
    (Theta^(-PHI)*Dist + PSI)/(1+PSI)*Output - Efficiency*Hours ;                                                                    % Eq. 12(11)
    -Output + Consumption ;                                                                                                          % Eq. 13(12)
    NominalInterestRate-(Inflation/Inflation_ss)^GAMMAPI*NominalInterestRate_ss ;                                                    % Eq. 14
    -Dist + (1-NU)*RelativePrice^(PHI/(1-PHI)) + NU*(Inflation_ss/Inflation)^(PHI/(1-PHI))*Dist_B ;                                  % Eq. 15(13)
    Exp1 - Lambda/Inflation ;                                                                                                        % Eq. 16
    Exp2 - Lambda*(Inflation_ss/Inflation)^(PHI/(1-PHI))*Z1 ;                                                                        % Eq. 17(14)
    Exp3 - Lambda*(Inflation_ss/Inflation)^(1/(1-PHI))*Z2 ;                                                                          % Eq. 18(15)
    Exp4 - Lambda*Inflation_ss/Inflation*Z3 ];                                                                                       % Eq. 19(16)

edx = {};
edx(1) = {[1,2]};    % Equations where the law of motion of the exogenous states are defined.
edx(2) = {[15,10]};  % Equations where the law of motion of the endogenous states are defined.
edx(3) = {16:19};    % Equations where the forward variables are defined.
edx(4) = { setdiff(1:length(DynamicModel),[edx{1},edx{2},edx{3}]) }; % Equations where the static variables are defined.
edx(5) = {[5:8]};    % Euler type equations.
edx(6) = {[14]};     % Taylor rule.

StaticModel  = compute_static_model(DynamicModel,variables,nn);

% Define the deterministic steady state (and some parameters).
steady_state_parameters = {};
steady_state_parameters = [ steady_state_parameters ; { 'Efficiency_ss' , 1.0}];
steady_state_parameters = [ steady_state_parameters ; { 'RiskPremium_ss' , 1.0}];
steady_state_parameters = [ steady_state_parameters ; { 'Inflation_ss' , 1.02}];
steady_state_parameters = [ steady_state_parameters ; { 'Hours_ss' , 1.0/3.0}];
steady_state_parameters = [ steady_state_parameters ; { 'Theta_ss' , 1.0}];
steady_state_parameters = [ steady_state_parameters ; { 'Dist_ss' , 1.0}];
steady_state_parameters = [ steady_state_parameters ; { 'RelativePrice_ss' , 1.0}];
steady_state_parameters = [ steady_state_parameters ; { 'NominalInterestRate_ss' , sym('Inflation_ss/BETA')}];
steady_state_parameters = [ steady_state_parameters ; { 'RealWage_ss' , sym('Efficiency_ss*(EPSILON-1)/EPSILON')}];
steady_state_parameters = [ steady_state_parameters ; { 'XIH' , sym('-(Efficiency_ss*Hours_ss)^(-SIGMAC)*RealWage_ss/Hours_ss^ETA')}];
steady_state_parameters = [ steady_state_parameters ; { 'Output_ss' , sym('Efficiency_ss*Hours_ss')}];
steady_state_parameters = [ steady_state_parameters ; { 'Consumption_ss' , sym('Output_ss')}];
steady_state_parameters = [ steady_state_parameters ; { 'Lambda_ss' , sym('Consumption_ss^(-SIGMAC)')}];
steady_state_parameters = [ steady_state_parameters ; { 'Z1_ss' , sym('RealWage_ss/Efficiency_ss*Output_ss/(1-NU*BETA)')}];
steady_state_parameters = [ steady_state_parameters ; { 'Z2_ss' , sym('Output_ss/(1-NU*BETA)')}];
steady_state_parameters = [ steady_state_parameters ; { 'Z3_ss' , sym('Output_ss/(1-NU*BETA)')}];
steady_state_parameters = [ steady_state_parameters ; { 'Exp1_ss' , sym('Lambda_ss/Inflation_ss')}];
steady_state_parameters = [ steady_state_parameters ; { 'Exp2_ss' , sym('RealWage_ss/Efficiency_ss*Output_ss/(1-NU*BETA)*Lambda_ss')}];
steady_state_parameters = [ steady_state_parameters ; { 'Exp3_ss' , sym('Output_ss/(1-NU*BETA)*Lambda_ss')}];
steady_state_parameters = [ steady_state_parameters ; { 'Exp4_ss' , sym('Output_ss/(1-NU*BETA)*Lambda_ss')}];

% Eval steady state parameters.
parameters = eval_steady_state_params(steady_state_parameters,parameters);

%% Generate a (Dynare) mod file.
if write_dynare_files
    zlb = 'NominalInterestRate';
    m2mod(variables,innovations,parameters,DynamicModel,edx,zlb,'calvo','../dyn');
end
%% Generate a steadystate file for dynare
if write_dynare_files
    m2ss(steady_state_parameters,'calvo','../dyn');
end
%% Check analytical steady state (model with a Taylor rule).
if ~check_steady_state(StaticModel,variables,innovations,parameters,nn,1)
    error('Steady state is wrong!')
else
    disp('Steady state check. Ok!')
end
