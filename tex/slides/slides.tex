\documentclass{beamer}

\usepackage{amssymb}
\usepackage{float}
\usepackage{setspace}
\usepackage{mathrsfs}
\usepackage{graphicx}
\usepackage{psfrag}
\usepackage{dcolumn}
\usepackage{multirow}
\usepackage{hyperref}
\usepackage{subfigure}
\usepackage{algorithms/algorithm}
\usepackage{algorithms/algorithmic}
\usepackage[english]{babel}
\usepackage[round]{natbib}
\usepackage[nice]{nicefrac}
\usepackage{fancybox}
\usepackage{multicol}
\usepackage{pgf}
\usepackage{tikz}

\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}

\newcolumntype{.}{D{.}{.}{-1}}
\newcolumntype{a}{D{.}{.}{10}}
\setlength{\parindent}{0pt}

\pdfcompresslevel=9

\hypersetup{
    bookmarks=false,
    unicode=true,
    pdftoolbar=false,
    pdfmenubar=false,
    pdffitwindow=true,
    pdfstartview={FitH},
    pdftitle={Accuracy of the Extended Path Simulation Metod in a New Keynesian Model with Zero Lower Bound on the Nominal Interest Rate},
    pdfauthor={Stéphane Adjemian and Michel Juillard},
    pdfproducer={pdflatex},
    colorlinks=true,
    linkcolor=blue,
    citecolor=blue,
    filecolor=blue,
    urlcolor=blue
}


% ----------------------------------------------------------------



% ---------------------------------------------------------------- %\maketitle % ----------------------------------------------------------------
\begin{document}

\author{Stéphane Adjemian (GAINS, Université du Maine and Cepremap), Michel Juillard (Bank of France and Cepremap)}
\title{Accuracy of the Extended Path Simulation Method in a New Keynesian Model with Zero Lower Bound on the Nominal Interest Rate}
\subtitle{prepared for ESRI-CEPREMAP workshop\\ Tokyo, February 7, 2011}
\date{}
\begin{frame}
  \titlepage
\end{frame}


\begin{frame}
\frametitle{Introduction}
\begin{itemize}
  \item ZLB implies a kink in the monetary policy function that makes solving a model with ZLB difficult.
  \item One possible approach is the extended path algorithm (Fair and Taylor, 1983) that, for each period, solves a deterministic perfect foresight problem where future shocks are set to zero.
  \item Approximation resides in violation of Jensen inequality.
  \item In this paper, we investigate the size of approximation errors for a New-Keynesian model with Calvo prices.
\end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Related literature}
  \begin{itemize}
  \item Gagnon (1990) and Love (2009) apply this approach to the neo--classical growth model and show high degree of accuracy for the extended path approach.
  \item However, the neo--classical growth model is not a very non-linear problem and there results may not carry to highly non--linear models such as the ones with ZLB.
  \end{itemize}
\end{frame}


\begin{frame}
\frametitle{Description of the model}
\begin{itemize}
\item Representative household
\item No capital accumulation
\item Monopolistic competition among producers of differentiated intermediary goods
\item Kimball aggregation of intermediary goods in a final good used for consumption and investment
\item Government issues lump-sum transferts to households
\item Monetary authority set short term nominal interest rate in response to inflation variation. It can't set negative nominal interest rates (ZLB)
\end{itemize}
\end{frame}

\begin{frame}
\frametitle{Households}

The inter-temporal  utility function of the representative household is given by:
\begin{equation*}
  \mathscr W_t = \mathscr U(C_t,h_t) + \beta \mathbb E_t \left[\mathscr W_{t+1}\right]
\end{equation*}
with
\begin{equation*}
  \mathscr U(C_t,h_t) = \frac{C_t^{1-\sigma_C}}{1-\sigma_C} - \xi_h \frac{h_t^{1+\eta}}{1+\eta}
\end{equation*}
Households maximize welfare under the following budget constraint:
\[
    P_t C_t  + \frac{B_{t}^d}{\varepsilon_{B,t} R_{t} } = B_{t-1}^d +
    W_th_t + \mathscr D_{t} + T_{t}
\]
\end{frame}



\begin{frame}
\frametitle{Final good (I)}
The final good is  obtained  by aggregating intermediate goods in a perfectly competitive sector.  The  representative  firm  use  a constant  return  to  scale
technology \emph{à la} \citet{KimballNBER1996}: 
\begin{equation*}
    \int_{0}^{1} \mathscr G\left(\frac{Y_t(z)}{Y_t}\right)dz = 1
\end{equation*}
where $\mathscr G$ is a strictly increasing concave function such that $\mathscr  G(1)=1$. We adopt the following functional form for this aggregation function \citep[see][]{DotseyKingFRBPhiladelphia2005,LevinLopezSalidoYun2007}:
\begin{equation*}
  \mathscr G(x) = \frac{\phi}{1+\psi}\left[(1+\psi)x-\psi\right]^{\frac{1}{\phi}}-\left[\frac{\phi}{1+\psi}-1\right]
\end{equation*}
where         $\phi=\frac{\varepsilon(1+\psi)}{\varepsilon(1+\psi)-1}$, $\psi\leq 0$ and $\varepsilon>0$.
\end{frame}

\begin{frame}
\frametitle{Final good (II)}
Given relative  prices, the  representative firm maximizes  its profit:

\begin{equation*}
  \label{eq:production:final:profit}
    \max_{\left\{Y_t(z); z\in[0,1]\right\}} Y_t - \int_{0}^{1}\frac{P_t(z)}{P_t} Y_t(z)\mathrm dz
\end{equation*}
subject to the technological constraints.
\end{frame}

\begin{frame}
\frametitle{Intermediary goods}

There   is   a   continuum    of   intermediate   goods   indexed   by $z\in[0,1]$. Each good  $z$ is produced by a  unique firm $z$ using labor. The
production technology is linear in the labor input:
\begin{equation*}
  \label{eq:production:intermediate:technology}
  Y_t(z) = A_t l_t(z).
\end{equation*}


Real marginal  cost is  invariant in  the cross section  of firms:
\[
mc_t =  \nicefrac{w_t}{A_t}
\] 
The nominal profit  of an intermediate firm that offers price $\mathcal P$ at date $t$ is given by:
\[
\Pi_t(\mathcal P) = \left(\frac{\mathcal
    P}{P_t}-mc_t\right)P_tY_t(z)
\]
\end{frame}

\begin{frame}
\frametitle{Calvo pricing}
Due   to  the   monopolistic  competition   between   firms  producing imperfectly substitutable intermediate goods, each firm $z$ has market
power. Nevertheless, a firm can't  decide of its optimal price in each period.  Following  a \cite{Calvo1983} scheme, at each  date, the firm
receives a signal telling it  whether it can revise its price $P_t(z)$ in an optimal  manner or not. 
\begin{itemize}
\item with probability $\nu$, firm $z$ can't  revise its price in a  given period and index its price according to:
\[
  P_t(z) = \pi^{\star} P_{t-1}(z)
\]
\item with probability $1-nu$, firm $z$ can re--optimize its price and   chooses price  $P_t(z)$ that  maximizes its profit knowing that it may not be able to re--optimize prices in the next periods.
\end{itemize}
\end{frame}

\begin{frame}
\frametitle{Government and monetary authority}
\begin{itemize}
\item The  government issues  lump  sum monetary  transfers,  $T_t$, to  the households and bonds. The government budget constraint is given by:
\[
  \frac{B_t^s}{\varepsilon_{B,t}R_t}-B_{t-1}^s-T_t = 0
\]
\item The central bank controls  the nominal interest rate according to the following rule:
\[
  R_t = \max\left\{1,R^{\star}\left(\frac{\pi_{t}}{\pi^{\star}}\right)^{r_{\pi}}\right\}
\]
\end{itemize}
\end{frame}

\begin{frame}
\frametitle{Price distortion}
{\small
Prices  in the  intermediary good  sector are  heterogeneous. However, this heterogeneity  doesn't hinder aggregation  because the technology
in   the   sector  of   intermediary   goods   is  linear.    Defining $l_t=\int_0^1l_t(z)d\mathrm z$  as the aggregate demand  for labor and
$\mathscr  Y_t=\int_0^1Y_t(z)d\mathrm  z$   the  sum  of  intermediary productions, we have directly:
\[
  \mathscr Y_t = A_t l_t
\]
Because             the             aggregation             technology
is not linear,  the sum of intermediary productions  is different from $Y_t$, the demand of final  good.  Integrating the demand function for
good  $z$ from  the final  good producer  over $z$, we get:
\[
  \mathscr Y_t = \Delta_{t} Y_t
\]
with the distortion $\Delta_t$ is defined as: 
\[
  \Delta_{t} \equiv \frac{1}{1+\psi}
  \int_0^1
  \left(
    \left(
      \frac{\nicefrac{P_t(z)}{P_t}}{\Sigma_t}
    \right)^{-(1+\psi)\varepsilon}
    + \psi
  \right)
  \mathrm dz
\]
where $\Sigma_t$ is the  Lagrange multiplier associated to the program  of the  representative  final good
producer.
}
\end{frame}

\begin{frame}
\frametitle{Dividends paid by intermediary good firms}
Firms in the intermediary good sector interact in monopolistic competition and make profits that are paid to households in the form of dividends. The sum of nominal profits at date $t$
is
\[
  \Pi_t = \int_0^1 \Pi_t(z)\mathrm dz
  =\int_0^1\left(\frac{P_t(z)}{P_t}-mc_t\right)P_tY_t(z)
  = P_t\left(Y_t - w_tl_t \right)
\]
As households own the firms, the profits are  paid to them. The repartition of these profits between the households is undetermined in general equilibrium, but we 
know that
\begin{equation*}\label{IntermediateFirms:Dividendes}
  \int_0^1\mathscr D_{1,t}(h)\mathrm dh = P_t\left(Y_t - w_tl_t \right)
\end{equation*}
\end{frame}

\begin{frame}
\frametitle{Equilibrium in labor and bond markets}

In general equilibrium, labor supply from the representative household equals aggregate labor demand by firms of the intermediary good market
and the demand and supply of bonds are equal:
\[
h_t = l_t, \quad B_t^d = B_t^s  
\]
\end{frame}

\begin{frame}
\frametitle{Equilibrium on the final good market}

By substituting the equilibrium condition on the bond market, the definition of aggregate dividends and the budget constraint of the government in the budget constraint of the representative household, we get:
\[
P_tC_t  =  W_{t} h_t +P_t\left(Y_t - w_tl_t\right)
\]
Knowing that the labor market is in equilibrium, we obtain:
\[
C_t  =  Y_t = \Delta_t^{-1} \mathscr Y_t
\]
\end{frame}

\begin{frame}
  \frametitle{Euler equations}
{\small
  \begin{align}
  \beta\varepsilon_{B,t} \mathbb E_t\mathscr E_{1,t+1}  - \frac{\lambda_t}{R_{t}} &= 0\\
  -\mathscr Z_{1,t} + \frac{w_t}{A_t}\Theta_{t}^{-\phi}Y_t + \nu\beta\frac{\mathbb E_t \mathscr E_{2,t+1}}{\lambda_t} &= 0\\
  -\mathscr Z_{2,t} + \Theta_{t}^{-\phi}Y_t + \nu\beta\frac{\mathbb E_t \mathscr E_{3,t+1}}{\lambda_t} &= 0\\
  -\mathscr Z_{3,t} + Y_t + \nu\beta\frac{\mathbb E_t \mathscr E_{4,t+1}}{\lambda_t} &= 0
\end{align}
with
\begin{align*}
  \mathscr E_{1,t} &= \frac{\lambda_t}{\pi_t}\\ 
  \mathscr E_{2,t} &= \lambda_t\left(\frac{\pi^{\star}}{\pi_t}\right)^{\frac{\phi}{1-\phi}}\mathscr Z_{1,t}\\ 
  \mathscr E_{3,t} &= \lambda_t\left(\frac{\pi^{\star}}{\pi_t}\right)^{\frac{1}{1-\phi}}\mathscr Z_{2,t}\\ 
  \mathscr E_{4,t} &= \lambda_t\frac{\pi^{\star}}{\pi_t}\mathscr Z_{3,t} 
\end{align*}
}
\end{frame}

\begin{frame}
\frametitle{Extended path approach}

The  model may be represented more generally as follows:
\begin{subequations}\label{eq:gmodel}
  \begin{equation*}\label{eq:gmodel:1}
    s_t = \mathsf Q (s_{t-1},u_t)
  \end{equation*}
  \begin{equation*}\label{eq:gmodel:2}
    \mathbb E_t \left[\mathsf F (y_t,x_t,x_{t-1},s_t,y_{t+1})\right] = 0  
  \end{equation*}
  \begin{equation*}\label{eq:gmodel:3}
    \mathsf G (y_t,x_t,x_{t-1},s_t) = 0  
  \end{equation*}
\end{subequations}
\end{frame}

\algsetup{
linenosize=\small,
linenodelimiter=.
}
\begin{frame}[fragile]
\frametitle{EP algorithm}
%\begin{algorithm}
%   \caption{Extended path algorithm}
%   \label{alg:ep}
   \begin{algorithmic}[1]
     \STATE $H \leftarrow$ Set the horizon of the perfect foresight models.
     \STATE $(x_0,s_1) \leftarrow$ Choose an initial condition for the state variables.
     \FOR{$t=1$ to $T$}
     \STATE $(y_t,z_t) \leftarrow$ Solve a perfect foresight model with terminal condition $y_{t+H}=y^{\star}$.
     \STATE $v   \leftarrow$ Draw independent uniform variates ($n_s\times 1$).
     \STATE $u   \leftarrow \mathrm P^{-1}(v)$
     \STATE $s_{t+1} \leftarrow \mathsf Q(s_t,u)$
     \ENDFOR
  \end{algorithmic}
%\end{algorithm}
\begin{itemize}
\item Main approximation: agents believe that  the innovations  of the  exogenous states  will be  zero  in the
future. There is no uncertainty about the future. 
\end{itemize}
\end{frame}

\begin{frame}
\frametitle{Remarks}
{\small
Advantages
\begin{itemize}
\item Makes it possible to simulate large models \emph{with  an arbitrary  precision} for their deterministic part.
\item No curse of dimension.
\item No special treatment required for occasionally binding constraints such as ZLB.
\end{itemize}
Main drawback
\begin{itemize}
\item We abstract from the effects of uncertainty
\end{itemize}
But 
\begin{itemize}
\item First order approximation also abstracts from uncertainty about the future and doesn't permit to handle ZLB
\item Second order approximation takes into account future uncertainty approximatively but doesn't permit to handle ZLB
\end{itemize}
}
\end{frame}

\begin{frame}
  \frametitle{Calibration of parameters}
\begin{center}
  \begin{tabular}{l|.}
    \hline\hline
    %Parameters & \text{Values} \\ \hline
    $\sigma_C$ & 1.50000\\
    $\eta$ & 2.00000\\
    $\beta$ & 0.99700\\
    $\nu$ & 0.75000\\
    $\psi$ & -0.10000\\
    $\epsilon$ & 6.00000\\
    $\gamma_{\pi}$ & 1.20000\\
    $\pi^{\star}$ & 1.00533\\
    $h^{\star}$ & 0.33300\\ \hline
    $\rho_a$ & 0.98000\\
    $\rho_b$ & 0.20000\\ \hline
    $\sigma_a^2$ & 0.00002\\
    $\sigma_b^2$ & 0.00001\\
    \hline\hline
  \end{tabular}
\end{center}
\end{frame}


\begin{frame}
  \frametitle{Comparing EP with perturbation approach at the ZLB (I)}
  \begin{figure}[H]
 \scalebox{0.6}{ 
  \input{../report0/comparison_with_zlb.pgf}
  }
  \caption{Comparison between EP and perturbations (for output paths, $Y_t$) when the ZLB is binding.}
\end{figure}
\end{frame}

\begin{frame}
  \frametitle{Comparing EP with perturbation approach at the ZLB (II)}
  \begin{figure}[H]
 \scalebox{0.6}{ 
  \input{../report0/comparison_without_zlb.pgf}
  }
  {Comparison between EP and perturbations (for output paths, $Y_t$) when the ZLB should be binding, but is not imposed.}
\end{figure}
\end{frame}

\begin{frame}
  \frametitle{Comparing EP with perturbation approach outside of  the ZLB}
  \begin{figure}[H]
 \scalebox{0.6}{ 
  \input{../report0/comparison_without_zlb2.pgf}
  }
  \caption{Comparison between EP and perturbations (for output paths, $Y_t$) when the ZLB is not binding.}
\end{figure}
\end{frame}

% \subsection{Numerical illustration}

% We  illustrate the EP  method by  comparing different  simulated paths obtained  with different  approaches: EP,  first and  second  (with or
% without  pruning) order  perturbations.  The  calibration of  the deep parameters is  described in table  \ref{tab:calibration}.  We simulate
% the   model,   as   defined   by   equations   (\ref{eq:model:1})   to (\ref{eq:model:19}), considering the  deterministic steady state as an
% initial condition and for all  the approaches we use the same sequence of (Gaussian) structural innovations. Results are presented in figures
% \ref{fig:comparison:binding_zlb}, \ref{fig:comparison:without_zlb} and \ref{fig:comparison:non_binding_zlb}.  The first remarkable feature is
% that the  second order perturbation  simulated paths (either  with our without pruning) are  closer to the EP simulated  paths than the first
% order perturbation simulated  paths. When the Zero Lower  Bound is not binding\footnote{Or when the nominal  interest rate is not negative if
%   we   do   not   impose    the   ZLB   constraint,   as   in   figure   \ref{fig:comparison:without_zlb}.}  (at  current time $t$  or in the
% expectations of  the agents)  the second order  perturbation simulated output is, on average, higher than the EP simulated output (0.017\% in
% terms of steady state level of output). Under the same conditions, the first order perturbation simulated  output is, on average, higher than
% the  EP  simulated  output  by  0.021\%. One  can  conclude  that  the discrepancies between EP and  perturbation approaches are fairly small
% when the ZLB is  not an issue. Figure \ref{fig:comparison:binding_zlb} shows that this  conclusion does not hold when the  ZLB is binding. In
% this situation the differences  between EP and perturbation approaches are much more  important, up to 8\% in terms of  steady state level of
% output.   Put differently, if  the EP  algorithm provides  an accurate solution\footnote{We  still have  to establish  this point. We  turn  to the
%   accuracy issue  in the  section \ref{sec:results}.}, it  means that using a perturbation  approach to  compute  forecasts  or  IRFs we  may
% overestimate the level of output or consumption by 8\% when the Zero Lower Bound is binding (or when the agents expect that the ZLB will 
% bind).\newline

% Figures \ref{fig:statedistribution:exogenous} and \ref{fig:statedistribution:endogenous} plot the distribution of the state variables, when 
% considering the EP approach. In figure \ref{fig:statedistribution:exogenous}, we clearly see that, with this model and its baseline calibration 
% described in table \ref{tab:calibration}, the ZLB binds in presence of large deflationary efficiency shocks. Overall, the probability of hitting 
% the ZLB is around 1\%. The volatility of the endogenous state variables is generally pretty small. Larger deviations from the steady state are 
% only observed when the nominal interest rate hits the ZLB. We would not observe such deviations if the paths were simulated with a perturbation
% approach\footnote{Note that with a first order perturbation approximation these two variables are constant, the endogenous state 
% variables vanish. Note also that in this model, the steady state levels of the state variables are on the boundary of the admissible 
% values for these variables (price distortion, $\Delta_t$, is greater or equal to one, and $\Omega_t$ is between zero and one). This can be   
% problematic because we are not able here to define the Taylor approximation on a open interval (and it is not clear if $k$-order approximations will deliver paths
% such that $\Delta_t\geq 1$ and $0\leq\Omega_t\leq 1$).}.\newline    

% In the next section we present, in a general framework, two approaches for  evaluating the  accuracy of  the EP  method with  respect  to the
% uncertainty about the future.

\begin{frame}
\frametitle{Accuracy checks}
{\small
\begin{itemize}
\item We  only
have to check  the accuracy of the solution with  respect to the Euler type equations, which can be rewritten as a multivariate integral:
\[
\mathscr R(x_-,s) \triangleq \int_{\Lambda} \mathsf F \left(y,x,x_{-},s,y+(u)\right) \mathrm d \mathrm P(u) = 0
\] 
where $\Lambda \subseteq \mathbb R^{n_s}$ and $(y,y_+(u),x,x_+(u))$ is provided  by the  EP algorithm  given  initial conditions
$(x_-,s)$.
\item The integral is computed by Gaussian quadrature, based on Hermite othogonal polynomials
\item \textbf{First accuracy test on a sphere}: with   state   variables  $(x_-,s)$   uniformly   distributed  on   an
hyper-sphere   centered    on   the   deterministic    steady   state, $(x^{\star},s^{\star})$. In  our case,  the two endogenous  states are
$\Theta_t$ and  $\Delta_t$. We  use a Quasi Monte-Carlo  approach  to  generate  points uniformly  distributed  in
$\mathscr   S_r$.
\item \textbf{Second accuracy test along a simulated path}:  On  each  point of  the
stochastic   simulation   we   compute  the   approximated   residuals $\widehat{\mathscr  R}(x_-,s)$, and  we report  various  moments.
\end{itemize}
}
\end{frame}
 
\begin{frame}
\frametitle{Accuracy on a growing sphere. The \cite{KimballNBER1996} curvature parameter is set to $\psi = -.1$.}
{\tiny 
 \begin{tabular}{l|l|aaaa}
    \hline\hline
    \multicolumn{1}{c|}{Radius} & \multicolumn{1}{c|}{Statistics} & \multicolumn{1}{c}{Equation 1} & \multicolumn{1}{c}{Equation 2} & \multicolumn{1}{c}{Equation 3} & \multicolumn{1}{c}{Equation 4}\\
    \hline
    \multirow{4}{*}{ $r=.0001$ } & max & 3.2050 \mbox{$\times 10^{-5}$} & 1.8572 \mbox{$\times 10^{-4}$} & 1.4756 \mbox{$\times 10^{-4}$} & 5.7833 \mbox{$\times 10^{-6}$} \\
     & min & 2.7845 \mbox{$\times 10^{-5}$} & 1.1276 \mbox{$\times 10^{-4}$} & 8.6854 \mbox{$\times 10^{-4}$} & 1.1623 \mbox{$\times 10^{-6}$} \\
     & mean & 2.8457 \mbox{$\times 10^{-5}$} &-1.1742 \mbox{$\times 10^{-4}$} & -9.0931\mbox{$\times 10^{-5}$} &-4.5901 \mbox{$\times 10^{-6}$} \\
     & median &2.8383 \mbox{$\times 10^{-5}$} & -1.1589 \mbox{$\times 10^{-4}$} & -8.9736 \mbox{$\times 10^{-5}$} & -4.8050 \mbox{$\times 10^{-6}$}\\ \hline
    \multirow{4}{*}{$r=.0010$} & max & 4.3484 \mbox{$\times 10^{-5}$} & 2.2876 \mbox{$\times 10^{-4}$} & 2.0068 \mbox{$\times 10^{-4}$} & 3.1793 \mbox{$\times 10^{-5}$}\\
     & min & 1.3478 \mbox{$\times 10^{-5}$} & 1.0419 \mbox{$\times 10^{-6}$} & 5.5564 \mbox{$\times 10^{-7}$} & 1.9791 \mbox{$\times 10^{-8}$} \\
     & mean & 2.7372 \mbox{$\times 10^{-5}$} & -1.1163 \mbox{$\times 10^{-4}$} & -8.5572 \mbox{$\times 10^{-5}$} & -4.5740 \mbox{$\times 10^{-6}$} \\
     & median & 2.7406 \mbox{$\times 10^{-5}$} & -1.1246\mbox{$\times 10^{-4}$} & -8.6391\mbox{$\times 10^{-5}$} & -4.4114 \mbox{$\times 10^{-6}$} \\ \hline
    \multirow{4}{*}{$r=.0100$} & max & 4.0789 \mbox{$\times 10^{-5}$} & 2.0103 \mbox{$\times 10^{-4}$} & 1.5842 \mbox{$\times 10^{-4}$} & 2.0934\mbox{$\times 10^{-5}$}\\
     & min & 9.4169 \mbox{$\times 10^{-6}$} & 3.9994 \mbox{$\times 10^{-5}$} & 2.6634 \mbox{$\times 10^{-5}$} & 8.1470 \mbox{$\times 10^{-9}$}\\
     & mean & 2.7994 \mbox{$\times 10^{-5}$} &  -1.2244 \mbox{$\times 10^{-4}$} & -9.3794 \mbox{$\times 10^{-5}$} & -4.4020 \mbox{$\times 10^{-6}$}\\
     & median &  2.7996 \mbox{$\times 10^{-5}$} &  -1.2288 \mbox{$\times 10^{-4}$} & -9.4145 \mbox{$\times 10^{-5}$} & -4.2716 \mbox{$\times 10^{-6}$}\\ \hline
    \multirow{4}{*}{$r=.1000$} & max & 2.7283\mbox{$\times 10^{-2}$} & 2.5795\mbox{$\times 10^{-1}$} & 2.1809\mbox{$\times 10^{-1}$} & 1.5645\mbox{$\times 10^{-2}$}\\
     & min & 3.7024 \mbox{$\times 10^{-6}$} & 1.6661 \mbox{$\times 10^{-6}$} & 2.2755 \mbox{$\times 10^{-6}$} & 2.0676 \mbox{$\times 10^{-8}$}\\
     & mean & 5.0364\mbox{$\times 10^{-4}$} & 1.3508\mbox{$\times 10^{-3}$} & 1.2159\mbox{$\times 10^{-3}$} & -4.5575\mbox{$\times 10^{-4}$}\\
     & median & 3.9968\mbox{$\times 10^{-5}$} & -1.4045\mbox{$\times 10^{-4}$}& -1.1126\mbox{$\times 10^{-4}$}& -4.4567\mbox{$\times 10^{-6}$} \\
    \hline\hline
  \end{tabular}
}
\end{frame}


\begin{frame}
\frametitle{Accuracy on a growing sphere. The \cite{KimballNBER1996} curvature parameter is set to $\psi = 5$.}
{\tiny
  \begin{tabular}{l|l|aaaa}
    \hline\hline
    \multicolumn{1}{c|}{Radius} & \multicolumn{1}{c|}{Statistics} & \multicolumn{1}{c}{Equation 1} & \multicolumn{1}{c}{Equation 2} & \multicolumn{1}{c}{Equation 3} & \multicolumn{1}{c}{Equation 4}\\
    \hline
    \multirow{4}{*}{ $r=.0001$ } & max & 1.9575 \mbox{$\times 10^{-5}$} & 3.9054 \mbox{$\times 10^{-5}$} & 8.0194 \mbox{$\times 10^{-5}$} & 8.8716 \mbox{$\times 10^{-7}$} \\
     & min & 1.9566 \mbox{$\times 10^{-5}$} & 3.8780 \mbox{$\times 10^{-5}$} & 7.9852 \mbox{$\times 10^{-5}$} & 8.8255 \mbox{$\times 10^{-7}$} \\
     & mean & 1.9571 \mbox{$\times 10^{-5}$} & -3.8912 \mbox{$\times 10^{-5}$} & -8.0020\mbox{$\times 10^{-5}$} & 8.8493 \mbox{$\times 10^{-7}$} \\
     & median &1.9571 \mbox{$\times 10^{-5}$} & -3.8911 \mbox{$\times 10^{-5}$} & -8.0019 \mbox{$\times 10^{-5}$} & 8.8495 \mbox{$\times 10^{-7}$}\\ \hline
    \multirow{4}{*}{$r=.0010$} & max & 1.9586 \mbox{$\times 10^{-5}$} & 4.4954 \mbox{$\times 10^{-5}$} & 8.6772 \mbox{$\times 10^{-5}$} & 9.0565 \mbox{$\times 10^{-7}$}\\
     & min & 1.9021 \mbox{$\times 10^{-5}$} & 3.8074 \mbox{$\times 10^{-5}$} & 7.8894 \mbox{$\times 10^{-5}$} & 8.5194 \mbox{$\times 10^{-7}$} \\
     & mean & 1.9525 \mbox{$\times 10^{-5}$} & -3.9333 \mbox{$\times 10^{-5}$} & -8.0495 \mbox{$\times 10^{-5}$} & 8.8504 \mbox{$\times 10^{-7}$} \\
     & median & 1.9570 \mbox{$\times 10^{-5}$} & -3.8935\mbox{$\times 10^{-5}$} & -8.0075\mbox{$\times 10^{-5}$} & 8.8550 \mbox{$\times 10^{-7}$} \\ \hline
    \multirow{4}{*}{$r=.0100$} & max & 2.0070 \mbox{$\times 10^{-5}$} & 6.2259 \mbox{$\times 10^{-5}$} & 1.0839 \mbox{$\times 10^{-4}$} & 1.1797\mbox{$\times 10^{-6}$}\\
     & min & 1.7309 \mbox{$\times 10^{-5}$} & 2.7800 \mbox{$\times 10^{-5}$} & 6.6044 \mbox{$\times 10^{-5}$} & 5.8211 \mbox{$\times 10^{-7}$}\\
     & mean & 1.9568 \mbox{$\times 10^{-5}$} &  -4.0297 \mbox{$\times 10^{-5}$} & -8.1875 \mbox{$\times 10^{-5}$} & 8.8350 \mbox{$\times 10^{-7}$}\\
     & median & 1.9566 \mbox{$\times 10^{-5}$} &  -3.9967 \mbox{$\times 10^{-5}$} & -8.1476 \mbox{$\times 10^{-5}$} & 8.8924 \mbox{$\times 10^{-7}$}\\ \hline
    \multirow{4}{*}{$r=.1000$} & max & 4.5064\mbox{$\times 10^{-3}$}  & 1.7567\mbox{$\times 10^{-2}$} & 3.1317\mbox{$\times 10^{-2}$} & 8.8448\mbox{$\times 10^{-4}$}\\
     & min & 1.6638\mbox{$\times 10^{-5}$} & 8.6023\mbox{$\times 10^{-9}$} & 1.5909 \mbox{$\times 10^{-5}$} &  2.3280 \mbox{$\times 10^{-9}$}\\
     & mean & 4.1831\mbox{$\times 10^{-4}$} & -1.2402\mbox{$\times 10^{-3}$} & -2.3660\mbox{$\times 10^{-3}$} & -6.8930\mbox{$\times 10^{-5}$}\\
     & median & 2.0337\mbox{$\times 10^{-5}$} & -3.8629\mbox{$\times 10^{-5}$}& -7.9544\mbox{$\times 10^{-5}$}& 8.4357\mbox{$\times 10^{-7}$} \\
    \hline\hline
  \end{tabular}
}
\end{frame}

\begin{frame}
\frametitle{Accuracy on a growing sphere. The \cite{KimballNBER1996} curvature parameter is set to $\psi = 10$.}
{\tiny 
 \begin{tabular}{l|l|aaaa}
    \hline\hline
    \multicolumn{1}{c|}{Radius} & \multicolumn{1}{c|}{Statistics} & \multicolumn{1}{c}{Equation 1} & \multicolumn{1}{c}{Equation 2} & \multicolumn{1}{c}{Equation 3} & \multicolumn{1}{c}{Equation 4}\\
    \hline
    \multirow{4}{*}{ $r=.0001$ } & max & 2.1558 \mbox{$\times 10^{-5}$} & 1.0827 \mbox{$\times 10^{-4}$} & 1.8376 \mbox{$\times 10^{-4}$} & 1.6306 \mbox{$\times 10^{-6}$} \\
     & min & 2.1456 \mbox{$\times 10^{-5}$} & 1.0655 \mbox{$\times 10^{-4}$} & 1.8173 \mbox{$\times 10^{-4}$} & 1.6209 \mbox{$\times 10^{-6}$} \\
     & mean & 2.1525 \mbox{$\times 10^{-5}$} & -1.0721 \mbox{$\times 10^{-4}$} & -1.8252\mbox{$\times 10^{-4}$} & 1.6262 \mbox{$\times 10^{-6}$} \\
     & median & 2.1531 \mbox{$\times 10^{-5}$} & -1.0715 \mbox{$\times 10^{-4}$} & -1.8245 \mbox{$\times 10^{-4}$} & 8.8495 \mbox{$\times 10^{-6}$}\\ \hline
    \multirow{4}{*}{$r=.0010$} & max & 2.1590 \mbox{$\times 10^{-5}$} & 1.4422 \mbox{$\times 10^{-4}$} & 2.2457 \mbox{$\times 10^{-4}$} & 1.6545 \mbox{$\times 10^{-6}$}\\
     & min & 1.8927 \mbox{$\times 10^{-5}$} & 1.0507 \mbox{$\times 10^{-4}$} & 1.7981 \mbox{$\times 10^{-4}$} & 2.0059 \mbox{$\times 10^{-6}$} \\
     & mean & 1.9525 \mbox{$\times 10^{-5}$} & -1.1019 \mbox{$\times 10^{-4}$} & -1.8590 \mbox{$\times 10^{-4}$} & 1.6231 \mbox{$\times 10^{-6}$} \\
     & median & 2.1504 \mbox{$\times 10^{-5}$} & -1.0850\mbox{$\times 10^{-4}$} & -1.8397\mbox{$\times 10^{-4}$} & 1.6268 \mbox{$\times 10^{-6}$} \\ \hline
    \multirow{4}{*}{$r=.0100$} & max & 2.2170 \mbox{$\times 10^{-5}$} & 1.5592 \mbox{$\times 10^{-4}$} & 2.4142 \mbox{$\times 10^{-4}$} & 2.0059\mbox{$\times 10^{-6}$}\\
     & min & 1.9897 \mbox{$\times 10^{-5}$} & 7.5671 \mbox{$\times 10^{-5}$} & 1.4339 \mbox{$\times 10^{-4}$} & 1.1669 \mbox{$\times 10^{-6}$}\\
     & mean & 2.1527 \mbox{$\times 10^{-5}$} &  -1.0930 \mbox{$\times 10^{-4}$} & -1.8513 \mbox{$\times 10^{-4}$} & 1.6219 \mbox{$\times 10^{-6}$}\\
     & median & 2.1556 \mbox{$\times 10^{-5}$} &  -1.0759 \mbox{$\times 10^{-4}$} & -1.8337 \mbox{$\times 10^{-4}$} & 1.6307 \mbox{$\times 10^{-6}$}\\ \hline
    \multirow{4}{*}{$r=.1000$} & max & 2.1522\mbox{$\times 10^{-3}$}  & 1.5787\mbox{$\times 10^{-2}$} & 2.3949\mbox{$\times 10^{-2}$} & 2.3646\mbox{$\times 10^{-4}$}\\
     & min & 1.8831\mbox{$\times 10^{-5}$} & 1.8681\mbox{$\times 10^{-7}$} & 2.5768 \mbox{$\times 10^{-5}$} &  7.7886 \mbox{$\times 10^{-9}$}\\
     & mean & 2.4846\mbox{$\times 10^{-4}$} & -1.3547\mbox{$\times 10^{-3}$} & -2.1297\mbox{$\times 10^{-3}$} & -2.1991\mbox{$\times 10^{-6}$}\\
     & median & 2.2908\mbox{$\times 10^{-5}$} & -1.2101\mbox{$\times 10^{-4}$}& -1.9152\mbox{$\times 10^{-4}$}& 1.1240\mbox{$\times 10^{-6}$} \\
    \hline\hline
  \end{tabular}
}
\end{frame}

\begin{frame}
\frametitle{Accuracy along a simulated path (8000 points)}
{\tiny
    \begin{tabular}{l|l|....}
    \hline\hline
    \multicolumn{1}{c|}{$\psi$} & \multicolumn{1}{c|}{Statistics} & \multicolumn{1}{c}{Equation 1} & \multicolumn{1}{c}{Equation 2} & \multicolumn{1}{c}{Equation 3} & \multicolumn{1}{c}{Equation 4}\\
    \hline
    \multirow{3}{*}{ $\psi=-.1$ } & max & 3.2003 & 25.9569 & 20.3840 & 32.8695 \\
     & mean & 0.0060 &  0.0946 & 0.0682 &  0.0270 \\
     & median & 0.0004 & 0.0404 & 0.0258 &  0.0014\\ \hline
    \multirow{3}{*}{$\psi=-5$} & max & 0.4472 & 7.6121 &  10.7019 & 0.3388 \\
     & mean & 0.0035 &  0.0633 &  0.0930 &  0.0023\\
     & median & 0.0010 & 0.0142 & 0.0241 &  0.0003\\ \hline
    \multirow{3}{*}{$\psi=-10$} & max & 0.1886 &  6.2691 & 8.0860 & 0.0838\\
     & mean & 0.0022 & 0.0939 & 0.1224 & 0.0011 \\
     & median & 0.0008 & 0.0388 & 0.0549 & 0.0005\\
    \hline\hline
  \end{tabular}\newline
Errors are represented as percentage of steady state output.
}
\end{frame}

\begin{frame}
  \frametitle{Conditional distribution of $C_t$}
  \begin{figure}[H]
 \scalebox{0.6}{ 
  \input{../report0/Consumption_conditional_density.pgf}
  }
  \caption{Conditional densities of consumption.}
\end{figure}
\end{frame}



\begin{frame}
\frametitle{References}
 %\bibliographystyle{plainnat}
 \begin{thebibliography}{4}
% \providecommand{\natexlab}[1]{#1}
% \providecommand{\url}[1]{\texttt{#1}}
% \expandafter\ifx\csname urlstyle\endcsname\relax
%   \providecommand{\doi}[1]{doi: #1}\else
%   \providecommand{\doi}{doi: \begingroup \urlstyle{rm}\Url}\fi
\bibitem[Calvo(1983)]{Calvo1983}
Guillermo~A. Calvo.
Staggered prices in a utility-maximizing framework.
\emph{Journal of Monetary Economics}, 12\penalty0 (3):\penalty0
  383--398, September 1983.
\bibitem[Dotsey and King(2005)]{DotseyKingFRBPhiladelphia2005}
Michael Dotsey and Robert~G. King.
Implications of state-dependent pricing for dynamic macroeconomic
  models.
Working Papers 05-2, Federal Reserve Bank of Philadelphia, February
  2005.
\bibitem[Kimball(1996)]{KimballNBER1996}
Miles~S. Kimball.
The quantitative analytics of the basic neomonetarist model.
NBER Working Papers 5046, National Bureau of Economic Research, March
  1996.
\bibitem[Levin et~al.(2007)Levin, Lopez-Salido, and
  Yun]{LevinLopezSalidoYun2007}
Andrew~T. Levin, J.~David Lopez-Salido, and Tack Yun.
Strategic complementarities and optimal monetary policy.
Kiel Working Papers 1355, Kiel Institute for the World Economy, June
  2007.
\end{thebibliography}
\end{frame}
\end{document}