function [ys,check,penlt] = calvo_linearized_steadystate(ys,exe)
global M_
persistent load_parameters update_parameters fill_ys

ys = zeros(M_.endo_nbr,1);
check = 0;

if isempty(load_parameters)
    load_parameters = [];
    for i = 1:rows(M_.param_names)
        load_parameters = [ load_parameters deblank(M_.param_names(i,:)) ' = M_.params(' int2str(i) ') ; ' ];
    end
end

if isempty(fill_ys)
    fill_ys = [];
    for i = 1:M_.endo_nbr
        fill_ys = [ fill_ys ' ys(' int2str(i)  ') = ' deblank(M_.endo_names(i,:)) '_ss; ' ];
    end
end

if isempty(update_parameters)
    idx = strmatch('XIH',deblank(M_.param_names),'exact');
    update_parameters = [ update_parameters ' M_.params(' int2str(idx) ')  = ' deblank(M_.param_names(idx,:)) '; ' ];
end

eval(load_parameters);


Efficiency_ss = 1;
RiskPremium_ss = 1;
Inflation_ss = 1+.5330/100;%(1.02)^(1/4);
Hours_ss = 0.33333;

Theta_ss = 1;
Dist_ss = 1;
RelativePrice_ss = 1;
NominalInterestRate_ss = Inflation_ss/BETA;
RealWage_ss = (Efficiency_ss*(EPSILON - 1))/EPSILON;
XIH = -RealWage_ss/(Hours_ss^ETA*(Efficiency_ss*Hours_ss)^SIGMAC);
Output_ss = Efficiency_ss*Hours_ss;
Consumption_ss = Output_ss;
Lambda_ss = 1/Consumption_ss^SIGMAC;
Z1_ss = -(Output_ss*RealWage_ss)/(Efficiency_ss*(BETA*NU - 1));
Z2_ss = -Output_ss/(BETA*NU - 1);
Z3_ss = -Output_ss/(BETA*NU - 1);
Exp1_ss = Lambda_ss/Inflation_ss;
Exp2_ss = -(Lambda_ss*Output_ss*RealWage_ss)/(Efficiency_ss*(BETA*NU - 1));
Exp3_ss = -(Lambda_ss*Output_ss)/(BETA*NU - 1);
Exp4_ss = -(Lambda_ss*Output_ss)/(BETA*NU - 1);


%dC_ss = 0;
%dY_ss = 0;
%LoggedInflation_ss = 100*log(Inflation_ss^4);
%LoggedInterestRate_ss = 100*log(NominalInterestRate_ss);

a_ss = 0;
b_ss = 0;

eval(update_parameters);

eval(fill_ys);