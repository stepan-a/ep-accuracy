function r = calvo_euler_residuals(y, params)
% Computes the residuals of the Euler type equations in calvo.mod given a path for the
% endogenous variables.
r = zeros(4, 1);
T75 = y(10)^(-params(11));
r(1) = y(6)*params(4)*y(26)/y(12)-1/y(15);
r(2) = y(19)-y(13)*y(16)/y(5)*T75-params(4)*params(5)*y(27)/y(12);
r(3) = y(20)-y(16)*T75-params(4)*params(5)*y(28)/y(12);
r(4) = y(21)-y(16)-params(4)*params(5)*y(29)/y(12);