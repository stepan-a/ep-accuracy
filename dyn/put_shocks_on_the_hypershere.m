function innovations = put_shocks_on_the_hypershere(varexo,autoregressive_parameters)
% stephane DOT adjemian AT ens DOT fr <18 janvier 2011>
innovations = zeros(1,2);
innovations(1) = log(varexo(1))+.5*variance_of_logged_productivity(autoregressive_parameters(1));
innovations(2) = log(varexo(2))+.5*variance_of_logged_risk_premium(autoregressive_parameters(2));