// File generated from model calvo at 16-Dec-2010 14:23:25

var Efficiency RiskPremium a b 
    Dist Theta 
    Consumption Lambda RealWage Inflation NominalInterestRate Output RelativePrice Hours 
    Z1 Z2 Z3 
    Exp1 Exp2 Exp3 Exp4 ;
    //dC dY LoggedInflation LoggedInterestRate;

varexo ea eb ;

parameters SIGMAC XIH ETA BETA NU PSI EPSILON RHOA RHOB GAMMAPI PHI ;

// CALIBRATION:
SIGMAC = 1.5;
ETA = 2;
BETA = 0.997;
NU = 0.75;
PSI = -.1;
EPSILON = 6;
PHI = EPSILON*(1+PSI)/(EPSILON*(1+PSI)-1);
RHOA = .98;
RHOB = .2;
GAMMAPI = 1.2;
XIH = 0; // This parameter will be updated in the steady state file.

/* REMARK:
**
** Deterministic steady state levels for inflation, hours, risk premium and efficiency are defined 
** in the steadystate file.
**
*/

// NOTE: The following external functions are used to compute the mean preserving spread terms. 
external_function(name = variance_of_logged_productivity,nargs=1);
external_function(name = variance_of_logged_risk_premium,nargs=1);

model;//(use_dll);

    [type = 'Definition of the exogenous state variables']
    Efficiency - STEADY_STATE(Efficiency)*exp(a-.5*variance_of_logged_productivity(RHOA));

    [type = 'Definition of the exogenous state variables']
    a - RHOA*a(-1) - ea ;

    [type = 'Definition of the exogenous state variables']
    RiskPremium - STEADY_STATE(RiskPremium)*exp(b-.5*variance_of_logged_risk_premium(RHOB));

    [type = 'Definition of the exogenous state variables']
    b - RHOB*b(-1) - eb ;

    [type = 'Definition of the non predetermined variables']
    Lambda - 1*Consumption^(-SIGMAC);

    [type = 'Definition of the non predetermined variables']
    Hours^ETA*XIH + Lambda*RealWage;

    [type = 'Euler equations']
    BETA*RiskPremium*Exp1(+1) - Lambda/NominalInterestRate;

    [type = 'Euler equations']
    Z1 - Output*RealWage/Efficiency*Theta^(-PHI) - BETA*NU*Exp2(+1)/Lambda;

    [type = 'Euler equations']
    Z2 - Output*Theta^(-PHI) - BETA*NU*Exp3(+1)/Lambda;

    [type = 'Euler equations']
    Z3 - Output - BETA*NU*Exp4(+1)/Lambda;

    [type = 'Definition of the non predetermined variables']
    Z2/((1+PSI)*(1-PHI))*RelativePrice^(PHI/(1-PHI))+Z3*PSI/(1+PSI)+Z1*PHI/((1+PSI)*(PHI-1))*RelativePrice^(PHI/(1-PHI)-1);      

    [type = 'Definition of the endogenous state variables']
    NU*Theta(-1)*(STEADY_STATE(Inflation)/Inflation)^(1/(1-PHI))+(1-NU)*RelativePrice^(1/(1-PHI)) - Theta;

    [type = 'Definition of the non predetermined variables']
    Theta^(1-PHI)-PSI-1 + PSI*RelativePrice*(1-NU) + STEADY_STATE(Inflation)/Inflation*NU*(1+PSI-Theta(-1)^(1-PHI));

    [type = 'Definition of the non predetermined variables']
    (Output*(PSI + Dist/Theta^PHI))/(PSI + 1) - Efficiency*Hours;

    [type = 'Definition of the non predetermined variables']
    Consumption - Output;

    [type = 'Taylor']
    //NominalInterestRate-max(1.0,STEADY_STATE(NominalInterestRate)*(Inflation/STEADY_STATE(Inflation))^GAMMAPI);
    NominalInterestRate-STEADY_STATE(NominalInterestRate)*(Inflation/STEADY_STATE(Inflation))^GAMMAPI;
    
    [type = 'Definition of the endogenous state variables']
    (Dist(-1)*NU)/(STEADY_STATE(Inflation)/Inflation)^(PHI/(PHI - 1)) - (NU - 1)/RelativePrice^(PHI/(PHI - 1)) - Dist;

    [type = 'Definition of the auxiliary forward variables']
    Exp1 - Lambda/Inflation;

    [type = 'Definition of the auxiliary forward variables']
    Exp2 - (Lambda*Z1)/(STEADY_STATE(Inflation)/Inflation)^(PHI/(PHI - 1));

    [type = 'Definition of the auxiliary forward variables']
    Exp3 - (Lambda*Z2)/(STEADY_STATE(Inflation)/Inflation)^(1/(PHI - 1));

    [type = 'Definition of the auxiliary forward variables']
    Exp4 - (STEADY_STATE(Inflation)*Lambda*Z3)/Inflation;

    /*
    ** Measurement equations
    */  

    //dC = 100*log(Consumption/Consumption(-1));
    //dY = 100*log(Output/Output(-1));
    //LoggedInflation = 400*log(Inflation);
    //LoggedInterestRate = 100*log(NominalInterestRate);
end;

shocks;
  var ea = .00002;
  var eb = .00001;
end;

steady;

options_.drop = 0;
options_.prunning = 0;
options_.replic = 1;

set_dynare_seed('default');
stoch_simul(order=2,irf=0,noprint,periods=0);

save('perturbation_reduced_form','oo_','options_','M_');