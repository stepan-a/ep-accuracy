function plot_distribution_of_states(v1,v2)
% stephane DOT adjemian AT ens DOT fr

T = 10000; % Length of the simulated paths.    
    
load('ep_with_zlb');

id_zlb = zlb_counter(find(zlb_counter(:,3)==1),1);
id_expected_zlb = zlb_counter(find(zlb_counter(:,3)>1),1);
id_without_zlb = setdiff(1:T,transpose([id_zlb;id_expected_zlb]));

plot(time_series_ep(v1,id_without_zlb),time_series_ep(v2,id_without_zlb),'.k');
hold on
plot(time_series_ep(v1,id_zlb),time_series_ep(v2,id_zlb),'or');
plot(time_series_ep(v1,id_expected_zlb),time_series_ep(v2,id_expected_zlb),'vg');
hold off
axis tight