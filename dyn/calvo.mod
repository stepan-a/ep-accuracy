// File generated from model calvo at 16-Dec-2010 14:23:25

// PSI = -.1 ==> Results stored in ep_with_zlb_0.mat
// PSI = -5  ==> Results stored in ep_with_zlb_1.mat
// PSI = -10 ==> Results stored in ep_with_zlb_2.mat
var Efficiency RiskPremium a b 
    Dist Theta 
    Consumption Lambda RealWage Inflation NominalInterestRate Output RelativePrice Hours 
    Z1 Z2 Z3 
    Exp1 Exp2 Exp3 Exp4 ;

varexo ea eb ;

parameters SIGMAC XIH ETA BETA NU PSI EPSILON RHOA RHOB GAMMAPI PHI ;

// CALIBRATION:
SIGMAC = 1.5;
ETA = 2;
BETA = 0.997;
NU = 0.75;
PSI = -.1;
EPSILON = 6;
PHI = EPSILON*(1+PSI)/(EPSILON*(1+PSI)-1);
RHOA = .98;
RHOB = .2;
GAMMAPI = 1.2;
XIH = 0; // This parameter will be updated in the steady state file.

/*
** Update the value of the Calvo probability if PSI is not equal to -.1.
*/

slope = 0.075074404761905; // from (EPSILON-1)/(EPSILON*(1-PSI)-1)*(1-NU)*(1-BETA*NU)/NU; with NU=.75 and PSI=-.1 (formula is obtained by linearizing the Phillips curve).
gamma = slope*(EPSILON*(1-PSI)-1)/(EPSILON-1);
discr = ((1+gamma+BETA)/BETA)^2-4/BETA;
NU    = .5*((1+gamma+BETA)/BETA-sqrt(discr));

/* REMARK:
**
** Deterministic steady state levels for inflation, hours, risk premium and efficiency are defined 
** in the steadystate file.
**
*/

// NOTE: The following external functions are used to compute the mean preserving spread terms. 
external_function(name = variance_of_logged_productivity,nargs=1);
external_function(name = variance_of_logged_risk_premium,nargs=1);

model(bytecode,block,cutoff=0);

    [type = 'Definition of the exogenous state variables']
    Efficiency - STEADY_STATE(Efficiency)*exp(a-.5*variance_of_logged_productivity(RHOA));

    [type = 'Definition of the exogenous state variables']
    RiskPremium - STEADY_STATE(RiskPremium)*exp(b-.5*variance_of_logged_risk_premium(RHOB));

    [type = 'Definition of the exogenous state variables']
    a - RHOA*a(-1) - ea ;

    [type = 'Definition of the exogenous state variables']
    b - RHOB*b(-1) - eb ;

    [type = 'Definition of the non predetermined variables']
    Lambda - 1*Consumption^(-SIGMAC);

    [type = 'Definition of the non predetermined variables']
    Hours^ETA*XIH + Lambda*RealWage;

    [type = 'Euler equations']
    BETA*RiskPremium*Exp1(+1) - Lambda/NominalInterestRate;

    [type = 'Euler equations']
    Z1 - Output*RealWage/Efficiency*Theta^(-PHI) - BETA*NU*Exp2(+1)/Lambda;

    [type = 'Euler equations']
    Z2 - Output*Theta^(-PHI) - BETA*NU*Exp3(+1)/Lambda;

    [type = 'Euler equations']
    Z3 - Output - BETA*NU*Exp4(+1)/Lambda;

    [type = 'Definition of the non predetermined variables']
    Z2/((1+PSI)*(1-PHI))*RelativePrice^(PHI/(1-PHI))+Z3*PSI/(1+PSI)+Z1*PHI/((1+PSI)*(PHI-1))*RelativePrice^(PHI/(1-PHI)-1);      

    [type = 'Definition of the endogenous state variables']
    NU*Theta(-1)*(STEADY_STATE(Inflation)/Inflation)^(1/(1-PHI))+(1-NU)*RelativePrice^(1/(1-PHI)) - Theta;

    [type = 'Definition of the non predetermined variables']
    Theta^(1-PHI)-PSI-1 + PSI*RelativePrice*(1-NU) + STEADY_STATE(Inflation)/Inflation*NU*(1+PSI-Theta(-1)^(1-PHI));

    [type = 'Definition of the non predetermined variables']
    (Output*(PSI + Dist/Theta^PHI))/(PSI + 1) - Efficiency*Hours;

    [type = 'Definition of the non predetermined variables']
    Consumption - Output;

    [type = 'Taylor']
    NominalInterestRate-max(1.0,STEADY_STATE(NominalInterestRate)*(Inflation/STEADY_STATE(Inflation))^GAMMAPI);
    //NominalInterestRate-STEADY_STATE(NominalInterestRate)*(Inflation/STEADY_STATE(Inflation))^GAMMAPI;
    
    [type = 'Definition of the endogenous state variables']
    (Dist(-1)*NU)/(STEADY_STATE(Inflation)/Inflation)^(PHI/(PHI - 1)) - (NU - 1)/RelativePrice^(PHI/(PHI - 1)) - Dist;

    [type = 'Definition of the auxiliary forward variables']
    Exp1 - Lambda/Inflation;

    [type = 'Definition of the auxiliary forward variables']
    Exp2 - (Lambda*Z1)/(STEADY_STATE(Inflation)/Inflation)^(PHI/(PHI - 1));

    [type = 'Definition of the auxiliary forward variables']
    Exp3 - (Lambda*Z2)/(STEADY_STATE(Inflation)/Inflation)^(1/(PHI - 1));

    [type = 'Definition of the auxiliary forward variables']
    Exp4 - (STEADY_STATE(Inflation)*Lambda*Z3)/Inflation;

end;


/*
** To reproduce the variances of output growth inflation and interest rate we need V(ea) = 0.00002 and V(eb) = 0.00001
** if we consider a first order approximation of the model.
**
** The non linear simulation (with EP approach) almost equal variance for these variables.
**/
shocks;
  var ea = .00002;  
  var eb = .00001;
end;

steady;

/*
**  Set various options.
*/
options_.verbose_mode = 1;
options_.dynatol.x = 1e-5;
options_.dynatol.f = 1e-5;

shocks;
  var  ea;
  periods 1;
  values -0.08;           
end;

Sigma = M_.Sigma_e;
M_.Sigma_e = M_.Sigma_e*0; 
simul(periods=350,stack_solve_algo=5);
M_.Sigma_e = Sigma;

/* REMARK. First compute the linear reduced form. This has to be (re)done if the deep parameters are changed. 
** The reduced form is used to initialize the 
**
**
** set_dynare_seed('default');
** options_.prunning = 0;       
** stoch_simul(order=2,irf=0,periods=1000);
** save('perturbation_reduced_form_1.mat','-mat');
**
*/

// Set the seed.
set_dynare_seed('default');

/*
** Trigger the Extended Path simulations.
*/
[time_series_ep,zlb_counter] = extended_path([],10000,350,'perturbation_reduced_form_2.mat',1,11);

/*
** Stochastic simulation with perturbation approach.
*/

taylor_reduced_form = load('perturbation_reduced_form_2.mat');

orig.oo_ = oo_;
orig.options_ = options_;
orig.M_ = M_;

oo_ = taylor_reduced_form.oo_;
options_ = taylor_reduced_form.options_;
M_ = taylor_reduced_form.M_;

// First order approximation:
set_dynare_seed('default');
options_.replic  = 1;
options_.order   = 1;
options_.periods = 10000;
time_series_taylor1 = simult(oo_.dr.ys,oo_.dr);
time_series_taylor1 = time_series_taylor1(:,1:end-1);

// Second order approximation (with prunning):
set_dynare_seed('default');
options_.replic  = 1;
options_.order   = 2;
options_.periods = 10000;
options_.pruning = 1;
time_series_taylor2p = simult(oo_.dr.ys,oo_.dr);
time_series_taylor2p = time_series_taylor2p(:,1:end-1);

// Second order approximation (with prunning):
set_dynare_seed('default');
options_.replic  = 1;
options_.order   = 2;
options_.periods = 10000;
options_.pruning = 0;
time_series_taylor2 = simult(oo_.dr.ys,oo_.dr);
time_series_taylor2 = time_series_taylor2(:,1:end-1);

oo_ = orig.oo_;
options_ = orig.options_;
M_ = orig.M_;

save('ep_with_zlb_2.mat');
