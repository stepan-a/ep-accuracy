// File generated from model calvo at 16-Dec-2010 14:23:25

addpath ../

var Efficiency RiskPremium a b 
    Dist Theta 
    Consumption Lambda RealWage Inflation NominalInterestRate Output RelativePrice Hours 
    Z1 Z2 Z3 
    Exp1 Exp2 Exp3 Exp4 ;

varexo ea eb ;

parameters SIGMAC XIH ETA BETA NU PSI EPSILON RHOA RHOB GAMMAPI PHI ;

// CALIBRATION:
SIGMAC = 1.5;
ETA = 2;
BETA = 0.997;
NU = 0.75;
PSI = -5;
EPSILON = 6;
PHI = EPSILON*(1+PSI)/(EPSILON*(1+PSI)-1);
RHOA = .98;
RHOB = .2;
GAMMAPI = 1.2;
XIH = 0; // This parameter will be updated in the steady state file.

/*
** Update the value of the Calvo probability if PSI is not equal to -.1.
*/

slope = 0.075074404761905; // from (EPSILON-1)/(EPSILON*(1-PSI)-1)*(1-NU)*(1-BETA*NU)/NU; with NU=.75 and PSI=-.1 (formula is obtained by linearizing the Phillips curve).
gamma = slope*(EPSILON*(1-PSI)-1)/(EPSILON-1);
discr = ((1+gamma+BETA)/BETA)^2-4/BETA;
NU    = .5*((1+gamma+BETA)/BETA-sqrt(discr));



/* REMARK:
**
** Deterministic steady state levels for inflation, hours, risk premium and efficiency are defined 
** in the steadystate file.
**
*/

// NOTE: The following external functions are used to compute the mean preserving spread terms. 
external_function(name = variance_of_logged_productivity,nargs=1);
external_function(name = variance_of_logged_risk_premium,nargs=1);

model(bytecode,block,cutoff=0);

    [type = 'Q']
    Efficiency = exp(a-.5*variance_of_logged_productivity(RHOA));//STEADY_STATE(Efficiency)*

    [type = 'Q']
    RiskPremium = exp(b-.5*variance_of_logged_risk_premium(RHOB)); //STEADY_STATE(RiskPremium)*

    [type = 'Q']
    a = RHOA*a(-1) + ea ;

    [type = 'Q']
    b = RHOB*b(-1) + eb ;

    [type = 'G']
    Lambda - 1*Consumption^(-SIGMAC);

    [type = 'G']
    Hours^ETA*XIH + Lambda*RealWage;

    [type = 'F']
    (BETA*RiskPremium*Exp1(+1)*NominalInterestRate)^(-1/SIGMAC)/STEADY_STATE(Consumption) - Consumption/STEADY_STATE(Consumption);

    [type = 'F']
    Z1/(RealWage/Efficiency*Theta^(-PHI))/STEADY_STATE(Output) - BETA*NU*Exp2(+1)/(Lambda*RealWage/Efficiency*Theta^(-PHI))/STEADY_STATE(Output) - Output/STEADY_STATE(Output);

    [type = 'F']
    Z2/Theta^(-PHI)/STEADY_STATE(Output) - BETA*NU*Exp3(+1)/(Lambda*Theta^(-PHI))/STEADY_STATE(Output) - Output/STEADY_STATE(Output);

    [type = 'F']
    Z3/STEADY_STATE(Output) - BETA*NU*Exp4(+1)/Lambda/STEADY_STATE(Output) - Output/STEADY_STATE(Output) ;

    [type = 'G']
    Z2/((1+PSI)*(1-PHI))*RelativePrice^(PHI/(1-PHI))+Z3*PSI/(1+PSI)+Z1*PHI/((1+PSI)*(PHI-1))*RelativePrice^(PHI/(1-PHI)-1);      

    [type = 'G']
    NU*Theta(-1)*(STEADY_STATE(Inflation)/Inflation)^(1/(1-PHI))+(1-NU)*RelativePrice^(1/(1-PHI)) - Theta;

    [type = 'G']
    Theta^(1-PHI)-PSI-1 + PSI*RelativePrice*(1-NU) + STEADY_STATE(Inflation)/Inflation*NU*(1+PSI-Theta(-1)^(1-PHI));

    [type = 'G']
    (Output*(PSI + Dist/Theta^PHI))/(PSI + 1) - Efficiency*Hours;

    [type = 'G']
    Consumption - Output;

    [type = 'G']
    NominalInterestRate-max(1.0,STEADY_STATE(NominalInterestRate)*(Inflation/STEADY_STATE(Inflation))^GAMMAPI);
    //NominalInterestRate-STEADY_STATE(NominalInterestRate)*(Inflation/STEADY_STATE(Inflation))^GAMMAPI;
    
    [type = 'G']
    (Dist(-1)*NU)/(STEADY_STATE(Inflation)/Inflation)^(PHI/(PHI - 1)) - (NU - 1)/RelativePrice^(PHI/(PHI - 1)) - Dist;

    [type = 'H']
    Exp1 - Lambda/Inflation;

    [type = 'H']
    Exp2 - (Lambda*Z1)/(STEADY_STATE(Inflation)/Inflation)^(PHI/(PHI - 1));

    [type = 'H']
    Exp3 - (Lambda*Z2)/(STEADY_STATE(Inflation)/Inflation)^(1/(PHI - 1));

    [type = 'H']
    Exp4 - (STEADY_STATE(Inflation)*Lambda*Z3)/Inflation;

end;


/*
** To reproduce the variances of output growth inflation and interest rate we need V(ea) = 0.00002 and V(eb) = 0.00001
** if we consider a first order approximation of the model.
**
** The non linear simulation (with EP approach) almost equal variance for these variables.
**/
shocks;
  var ea = .00002;  
  var eb = .00001;
end;

steady;

/*
**  Set various options.
*/
options_.verbose_mode = 0;
options_.dynatol.x = 1e-5;
options_.dynatol.f = 1e-5;

Sigma = M_.Sigma_e;
M_.Sigma_e = M_.Sigma_e*0; 
simul(periods=800,stack_solve_algo=5);
M_.Sigma_e = Sigma;

/* REMARK. First compute the linear reduced form. This has to be (re)done if the deep parameters are changed. 
** The reduced form is used to initialize the 
**
** 
** set_dynare_seed('default');
** options_.prunning = 0;       
** stoch_simul(order=2,irf=0);
** save('perturbation_reduced_form.mat','-mat');
**
*/

/*
** Accuracy analysis 
*/

info.hermite_order = 5;                                // Order of approximation for the computation of the expectations. 

info.ns = 2;                                           // Number of exogenous state variables.
info.nx = 2;                                           // Number of endogenous state variables.
info.ia = find(M_.lead_lag_incidence(1,:)>0);          // Indices of the state variables.
info.is = info.ia(1:info.ns);                          // Indices for the exogenous state variables (in logs).
info.ix = info.ia(info.ns+1:info.ns+info.nx);          // Indices for the endogenous state variables.
info.iS = 1:info.ns;                                   // Indices for the exogenous state variables (in levels).

tmp = transpose(M_.lead_lag_incidence);
tmp = tmp(:);
info.iv = find(tmp) ;                                  // Indices for the (lagged, contemporaneaous and leaded) variables.

info.ne = 4;                                           // Number of Euler type equations.
info.ie = 7:10;                                        // Indices for the Euler type equations. 

info.positivity_constraint = [1];                        // First endogenous state variable (Dist) has to be greater than its steady state level (1).
info.negativity_constraint = [2];                        // Second endogenous state variable (Theta) has to be smaller than its steady state level (1).

info.nv = M_.endo_nbr;                                 // Number of endogenous variables.

info.parameters = M_.params;                           // Values of the deep parameters.

info.steadystate = oo_.steady_state;                   // Steady state, will be used as the center of the hypershere for the state variables.
info.exogenous_process_parameters = M_.params(8:9);    // Autoregressive parameters, used to compute the variance of the exogenous state variables.
info.innovation_variances = diag(M_.Sigma_e);          // Variances of the innovations of the exogenous state variables. 
info.periods = 800;

tmp = load('../perturbation_reduced_form_1');              // Load the perturbation reduced form solution.
info.dr = tmp.oo_.dr;                                   // Copy the perturbation reduced form into info.

info.euler_equations_routine = 'calvo_euler_residuals';

datastruct = load('../ep_with_zlb_1_800');

results_psi_equal_minus_5_800 = ep_accuracy_along_a_path(datastruct.time_series_ep,8000,1000,info);

save('calvo_path_accuracy_psi5_800.mat');